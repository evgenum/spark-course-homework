package course.spark.homework2.inputsource

import java.io.IOException
import java.util.concurrent.TimeUnit

import org.apache.spark.internal.Logging
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.receiver.Receiver

class WikipediaEditReceiver(
    channel: String = "#en.wikipedia",
    host: String = "irc.wikimedia.org",
    port: Int = 6667,
    pollTimeoutMillis: Long = 100)
  extends Receiver[WikipediaEditEvent](StorageLevel.MEMORY_AND_DISK_2)
  with Logging {

  def onStart() {
    // Start the thread that receives data over a connection
    new Thread("Socket Receiver") {
      override def run() { receive() }
    }.start()
  }

  def onStop() {
    // There is nothing much to do as the thread calling receive()
    // is designed to stop by itself if isStopped() returns false
  }

  /** Create a socket connection and receive data until receiver is stopped */
  private def receive() {
    val ircStream = new WikipediaEditEventIrcStream(host, port)
    var edit: WikipediaEditEvent = null

    try {
      ircStream.connect()
      ircStream.join(channel)

      while(!isStopped) {
        edit = ircStream.getEdits.poll(pollTimeoutMillis, TimeUnit.MILLISECONDS)
        if (edit != null)
          store(edit)
      }

      ircStream.leave(channel)
      ircStream.close()

      // Restart in an attempt to connect again when server is active again
      restart("Trying to connect again")
    } catch {
      case e: IOException =>
        // restart if could not connect to server
        restart("Error connecting to " + host + ":" + port, e)
      case t: Throwable =>
        // restart if there is any other error
        restart("Error receiving data", t)
    }
  }
}